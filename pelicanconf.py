#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Kresus'
SITENAME = u'Kresus'
SITEURL = ''

PATH = 'content'
PLUGIN_PATHS = ['plugins']
PLUGINS = ['toc', 'i18n_subsites']

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'
LOCALE = ('fr_FR.UTF-8',)

I18N_SUBSITES = {
    'fr': {},
    'en': {}
}

# To show the full language word
languages_lookup = {
    'fr' : 'Français',
    'en' : 'English'
}

def lookup_lang_name(lang_code):
    return languages_lookup[lang_code]

JINJA_FILTERS = {
    'lookup_lang_name': lookup_lang_name,
}

JINJA_ENVIRONMENT = {
    "extensions": ['jinja2.ext.i18n']
}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 10

SUMMARY_MAX_LENGTH = 45

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

THEME = 'themes/custom'

DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = False

STATIC_PATHS = ['images']

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.toc': {'anchorlink': True},
        'markdown.extensions.codehilite': {'css_class': 'highlight'}
    },
    'output_format': 'html5'
}

LOAD_CONTENT_CACHE = False

# put articles (posts) in blog/
ARTICLE_URL = 'blog/{slug}.html'
ARTICLE_SAVE_AS = 'blog/{slug}.html'
# we need to change the main index page now though...
INDEX_SAVE_AS = 'blog/index.html'
INDEX_URL = 'blog/'
#now move all the category and tag stuff to that blog/ dir as well
CATEGORY_URL = 'blog/category/{slug}.html'
CATEGORY_SAVE_AS = 'blog/category/{slug}.html'
CATEGORIES_URL = 'blog/category/'
CATEGORIES_SAVE_AS = 'blog/category/index.html'
TAG_URL = 'blog/tag/{slug}.html'
TAG_SAVE_AS = 'blog/tag/{slug}.html'
TAGS_URL = 'blog/tag/'
TAGS_SAVE_AS = 'blog/tag/index.html'
ARCHIVES_SAVE_AS = 'blog/archives/archives.html'
ARCHIVES_URL = 'blog/archives/archives.html'
AUTHOR_SAVE_AS = 'blog/{slug}.html'
AUTHORS_SAVE_AS = 'blog/authors.html'
# put pages in the root directory
PAGE_SAVE_AS = '{slug}.html'
PAGE_URL = '{slug}.html'

# Fathom analytics.
FATHOM_URL = '1984.b.delire.party'
FATHOM_SITE_ID = 'RFNVO'

TOC = {
    'TOC_HEADERS': '^h[1-6]',
    'TOC_RUN': 'false'
}

DEFAULT_DATE_FORMAT = '%d/%m/%Y'
