Title: Installation
Date: 2017-03-11 10:02
Slug: install
Summary: Installation
toc_run: true
toc_title: Comment installer Kresus&nbsp;?
lang: fr


## Tutoriels de la communauté

Tout d'abord, remercions tous les membres de la communauté pour tous les
tutoriels complets et très bien documentés expliquant comment installer Kresus
dans des conditions particulières !

- [fr] [Gérer ses finances avec Kresus](https://wiki.bruno-tatu.com/doku.php?id=wiki:install-kresus)
- [fr] [Installer Kresus sur Debian 8](https://carmagnole.ovh/tuto-installer-kresus.htm)
- [fr] [Kresus, un gestionnaire Web de finances personnelles](https://blog.karolak.fr/post/2016-03-18-kresus/)
- [en] [Surveiller vos comptes bancaires avec une Raspberry PI](https://phyks.me/2017/07/tracking-your-bank-accounts-with-a-raspberry-pi-kresus.html)

Si vous écrivez un tutoriel lié à l'installation ou l'utilisation de Kresus,
n'hésitez-pas à nous contacter et nous serons ravis de l'inclure dans cette
liste !

## Installation avec Docker

### Attention : à lire avant de lancer une image docker (pré-construite ou non)

Par défaut docker [expose le port 9876 à `0.0.0.0`](https://docs.docker.com/engine/userguide/networking/default_network/binding/), rendant ainsi votre Kresus visible par tous.
Si vous êtes derrière un reverse-proxy tel que nginx ou apache, préférez `-p 127.0.0.1:9876:9876` pour rendre visible Kresus uniquement à votre reverse-proxy.
Cette documentation utilisera cependant `-p 9876:9876` dans tous les exemples ci-dessous, afin de rester générique.

### Lancez une image pré-construite

L'image Docker a l'avantage d'inclure toutes les dépendances nécessaires, dont
une installation de Weboob complète. A chaque redémarrage de l'image, Weboob
essayera de se mettre à jour ; si vous rencontrez donc un problème de modules,
il est recommandé de simplement redémarrer l'image (un simple `restart`
suffit).

L'image pré-construite expose plusieurs volumes de données. Il n'est pas
obligatoire de les monter, mais cela vous permet de conserver vos données
personnelles entre les redémarrages de l'image Docker.

- `/home/user/data` contient toutes les données utilisées par Kresus. Il est
  recommandé de monter ce volume, pour éviter des pertes de données entre
  chaque nouveau démarrage de l'image (par exemple, après une mise à jour).
- `/weboob` contient le clone local de Weboob installé au démarrage. L'exposer
  au système local n'est pas obligatoire, mais permet de :
    - mutualiser les répertoires d'installation de Weboob, si plusieurs images
      Kresus sont instanciées. Cela permet notamment des économies d'espace
      disque.
    - mettre en place des `crontab` sur la machine hôte, qui se chargeront de
      mettre à jour Weboob régulièrement (avec un `git pull` depuis le
      répertoire de Weboob sur l'hôte).
- `/opt/config.ini` contient le fichier de configuration de kresus. L'exposer
  n'est pas obligatoire et ne devrait être fait que si vous voulez changer des
  options. Voir plus d'explications sur [le fichier de
  configuration](#avec-un-fichier-configini).

**Note** : si vous clonez Weboob, il vous est suggéré d'utiliser la branche *master* (développement) qui devrait être plus à jour que la branche *stable*.

La variable d'environnement suivante peut être définie :

- `LOCAL_USER_ID` permet de choisir l'UID de l'utilisateur interne de l'image
  Docker (afin de ne pas faire que kresus tourne en root dans l'image).

Voici un exemple de ligne de commande pour lancer Kresus en production dans une
image Docker, avec le même utilisateur UNIX que l'actuel :

    :::bash
    mkdir -p /opt/kresus/data
    mkdir -p /opt/kresus/weboob
    touch /opt/kresus/config.ini
    git clone https://git.weboob.org/weboob/weboob.git /opt/kresus/weboob

    docker run -p 9876:9876 \
        -e LOCAL_USER_ID=`id -u` \
        --restart unless-stopped \
        -v /opt/kresus/data:/home/user/data \
        -v /opt/kresus/weboob:/weboob \
        -v /opt/kresus/config.ini:/opt/config.ini \
        --name kresus \
        -ti -d bnjbvr/kresus

### Utilisation de docker-compose

Le répertoire docker-compose comprend tous les éléments pour lancer facilement kresus :

- un reverse proxy ([traefik](https://traefik.io/))
- un serveur postfix pour envoyer les mails
- kresus

Les fonctionnalités sont :

- chiffrement ssl automatique avec [letsencrypt](https://letsencrypt.org/).
- protection par login/mot de passe.

Prérequis :

- un serveur public avec un nom de domaine (FQDN), avec les ports 80 et 443
  ouverts (pas de apache ou de nginx).
- avoir [docker-compose](https://docs.docker.com/compose/install/) installé
  (version >= 1.19).

#### Méthodologie

- générer un password avec htpasswd :

        htpasswd -bn my_user my_password |awk -F':' '{print $2}'

- renommer le fichier `docker-compose-example.env` en `.env` (il doit rester
  dans le répertoire docker-compose).
- éditer le fichier `.env` en remplaçant toutes les variables d'environnement
  par des valeurs adéquates :
    - `MY_DOMAIN` : le nom complet du serveur (ex : mamachine.mondomaine.net)
    - `MAIL_ADDRESS` : adresse mail pour recevoir les alertes letsencrypt
    - `MY_USERNAME` : nom d'utilisateur pour l'authentification
    - `MY_PASSWD` : mot de passe pour l'authentification, obtenu lors de la première étape
- renommer le fichier docker-compose.example.yml en docker-compose.yml
- lancer docker-compose:

        docker-compose up -d

### Construire l'image soi-même

#### Image stable

L'image correspondant à la dernière version stable de Kresus peut être
téléchargée via le *hub* de Docker. Il s'agit de l'image accessible via
`bnjbvr/kresus`.

Il est possible de la reconstruire à la main. Vous aurez besoin de `nodejs` (de
préférence la version LTS ou une version plus récente) et de `npm` pour
reconstruire l'image de zéro.

    :::bash
    git clone https://framagit.org/kresusapp/kresus && cd kresus
    docker build -t myself/kresus -f docker/Dockerfile-stable .

Vous pouvez ensuite l'utiliser:

    :::bash
    docker run -p 9876:9876 -v /opt/kresus/data:/home/user/data -ti -d myself/kresus

#### Image _Nightly_

Il existe aussi une image _Nightly_, avec les derniers changements, construite
chaque nuit à partir du dépôt Git. Attention, cette image est expérimentale et
peut contenir de nombreux bugs ou corrompre vos données, car la branche
_master_ peut être instable de temps en temps. Cette image récupèrera les
dernières sources depuis le dépôt Git en ligne et n'utilisera donc pas de
sources locales.

Pour construire une image _Nightly_ compilée pour une utilisation en
production:

    :::bash
    make docker-nightly

Cette commande construira une image nommée `bnjbvr/kresus-nightly`.

Pour la lancer ensuite, vous pouvez utiliser la même commande `docker run` que
précédemment, en adaptant le nom de l'image:

    :::bash
    docker run -p 9876:9876 \
        -e LOCAL_USER_ID=`id -u` \
        --restart unless-stopped \
        -v /opt/kresus/data:/home/user/data \
        -v /opt/kresus/weboob:/weboob \
        -v /opt/kresus/config.ini:/opt/config.ini \
        --name kresus \
        -ti -d bnjbvr/kresus-nightly


## Installation dans Yunohost

Une [application](https://github.com/YunoHost-Apps/kresus_ynh) pour
[Yunohost](http://yunohost.org/) est disponible. Celle-ci est encore récente
et expérimentale, n'hésitez pas à nous signaler tout problème que vous
pourriez rencontrer.


## Installation dans ArchLinux

Vous pouvez utiliser `pacman` pour installer [Kresus pour ArchLinux](https://www.archlinux.org/packages/community/x86_64/kresus/):

```bash
pacman -Syu kresus
```


## Pré-requis pour les autres installations

Kresus utilise [Weboob](http://weboob.org/) sous le capot, pour se connecter au
site web de votre banque. Vous aurez besoin d'[installer le cœur et les modules
Weboob](http://weboob.org/install) afin que l'utilisateur exécutant Kresus
puisse les utiliser.

Kresus nécessite la dernière version stable de Weboob. Bien que Kresus puisse
fonctionner avec de précédentes versions, les modules des banques peuvent être
obsolètes, et la synchronisation avec votre banque pourrait être
dysfonctionnelle.


## Installation autonome

**AVERTISSEMENT: Il n'y a aucun système d'authentification intégré dans Kresus,
il est donc risqué de l'utiliser tel quel. Assurez-vous bien de mettre en place
un système d'authentification vous-même…**

Cela installera les dépendances Node, construira le projet et installera le
programme dans le répertoire node.js global. Notez que si cet emplacement est
`/usr/local/bin`, vous devrez probablement lancer cette commande en tant que
root.

### Installation locale

Pour installer les dépendances node et compiler les scripts (cela
n'installera pas Kresus globalement) pour un usage en production :

    :::bash
    npm install && npm run build:prod

Vous pourrez alors lancer Kresus en utilisant

    :::bash
    NODE_ENV=production npm run start

_Note :_ Vous pouvez aussi utiliser `npm run build:dev` pour compiler les
scripts en mode de développement (mieux pour débugger mais inadapté pour un
usage en production).


## Installation globale

Autrement, si vous souhaitez installer Kresus globalement vous utiliserez :

    :::bash
    make install

Et pourrez ensuite simplement lancer Kresus depuis n'importe quel terminal
depuis n'importe quel répertoire avec :

    :::bash
    kresus


## Configuration

### Avec un fichier `config.ini`

Vous pouvez définir toutes les options à utiliser dans un fichier `INI` et
passer l'argument `-c path/to/config.ini` à Kresus au lancement. Un fichier
`config.ini.example` est disponible dans le dépôt Git pour lister les options
disponibles. Il peut être copié et édité pour mieux correspondre à vos choix.

**Sécurité :** En mode production (`NODE_ENV=production`), si le fichier de
configuration ne fournit pas uniquement les droits de lecture ou
lecture/écriture à son propriétaire, en utilisant les permissions du système
de fichier, Kresus refusera de démarrer.


### Avec des variables d'environnement

Notez que chaque option de configuration a une variable d'environnement
associée : si la variable d'environnement est définie, elle surchargera la
configuration du fichier `INI` ou la valeur par défaut. Référez-vous au
fichier `config.ini.example` pour trouver toutes les variables d'environnement
utilisables.


## Recommandations pour le pare-feu

Vous devrez définir les autorisations suivantes :

- Accès http/https au site web de votre banque, pour récupérer les nouvelles
  opérations.
- Accès http/https aux dépôts Weboob, pour la mise à jour automatique des
  modules avant les rappatriements automatiques, si vous utilisez la
  fonctionnalité de mise à jour automatique.
