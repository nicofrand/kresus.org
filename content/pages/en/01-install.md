Title: Installation
Date: 2017-03-11 10:02
Slug: install
Summary: Installation
toc_run: true
toc_title: How to install Kresus&nbsp;?
lang: en


## Community tutorials

First of all, let's thank our community for making well-documented and very
complete tutorials explaining how to install Kresus under different settings!

- [en] [Tracking your bank accounts with a Raspberry Pi](https://phyks.me/2017/07/tracking-your-bank-accounts-with-a-raspberry-pi-kresus.html)
- [fr] [Manage your finances with Kresus](https://wiki.bruno-tatu.com/doku.php?id=wiki:install-kresus)
- [fr] [Kresus, a Web personal finance manager](https://blog.karolak.fr/post/2016-03-18-kresus/)
- [fr] [Install Kresus on Debian 8](https://carmagnole.ovh/tuto-installer-kresus.htm)

If you write any tutorial that would be related to Kresus, let's get in touch:
we would be certainly very happy to include it in this list!

## Install with Docker

### Warning: read before running any docker image

By default docker [exposes the port 9876 to `0.0.0.0`](https://docs.docker.com/engine/userguide/networking/default_network/binding/), which will make your Kresus instance exposed to the whole world.
If you are behind a reverse-proxy such as nginx or apache, prefer `-p 127.0.0.1:9876:9876` to make it visible to your reverse-proxy only. This documentation will however use `-p 9876:9876` in all examples below, to remain generic.

### Run a pre-built image

The Docker images already bundles all the required dependencies, including the
full Weboob install. At each boot, Weboob will try to update; if you have any
problem with modules you should then try to simply `restart` the image.

This pre-built image exposes several data volumes. It is not mandatory to mount
them, but it would let you persist your personal data across reboots of the
Docker image.

- `/home/user/data` stores all data used by Kresus. It is recommended to mount
  this volume, to avoid losing data at each restart of the image (for instance
  after an update).
- `/weboob` stores the local Weboob clone, set up at startup. It is not
  mandatory to expose it to the local filesystem, but this would allow you to
    - share Weboob installation folders across scripts and Kresus instances,
      resulting in less disk space being used.
    - set some `crontab` on the host machine to regularly update Weboob
      completely (with a `git pull` in the Weboob folder from the host).
- `/opt/config.ini` is a configuration file. It is not mandatory, and should be
  exposed only if you want to change kresus options. You can find more
  information about the configuration file
  [here](#using-a-configini-file).

**Note**: if you clone Weboob, it is suggested to use the *master* (development) branch, which is more likely to be up to date than the *stable* branch.

The following environment variable can be used:

- `LOCAL_USER_ID` selects the UID of the user inside the Docker image (for
  preventing Kresus from running as root).

Here is an example of command line to run to start Kresus in production in a
Docker image, with the same UNIX user as current:

    :::bash
    mkdir -p /opt/kresus/data
    mkdir -p /opt/kresus/weboob
    touch /opt/kresus/config.ini
    git clone https://git.weboob.org/weboob/weboob.git /opt/kresus/weboob

    docker run -p 9876:9876 \
        -e LOCAL_USER_ID=`id -u` \
        --restart unless-stopped \
        -v /opt/kresus/data:/home/user/data \
        -v /opt/kresus/weboob:/weboob \
        -v /opt/kresus/config.ini:/opt/config.ini \
        --name kresus \
        -ti -d bnjbvr/kresus

### Use docker-compose

The docker-compose directory contains all elements to easily run kresus:

- a reverse proxy ([traefik](https://traefik.io/))
- a postfix server to send mails
- kresus

Functionalities:

- automatic SSL encryption with [letsencrypt](https://letsencrypt.org/).
- login/password access to kresus.

Prerequisites:

- a public server, with a fully qualified domain name, having ports 80 and 443
  open (no running apache or nginx).
- [docker-compose](https://docs.docker.com/compose/install/) must be installed
  (version >= 1.19).

#### Method

- create a password with htpasswd :

        htpasswd -bn my_user my_password | awk -F':' '{print $2}'

- rename the `docker-compose-example.env` file to `.env` (it must be in the
  docker-compose directory).
- edit `.env` file, replacing all vars:
    - `MY_DOMAIN` : server fully qualified domain name (for instance:
      myserver.mydomain.net).
    - `MAIL_ADDRESS` : mail address to receive letsencrypt alerts.
    - `MY_USERNAME` : username for authentication.
    - `MY_PASSWD` : username for authentication, the one you got on the first
      step.
- rename the  `docker-compose.example.yml` to `docker-compose.yml`.
- Then launch docker-compose :

         docker-compose up -d

### Build the Docker image

#### Stable image

The image for the latest stable version of Kresus can be downloaded through
the Docker hub. This is the image called `bnjbvr/kresus`.

You can also build it yourself. You will need `nodejs` (preferably LTS version
or later) and `npm` to build the image from scratch.

    :::bash
    git clone https://framagit.org/kresusapp/kresus && cd kresus
    docker build -t myself/kresus -f docker/Dockerfile-stable .

You can then use it:

    :::bash
    docker run -p 9876:9876 -v /opt/kresus/data:/home/user/data -ti -d myself/kresus

#### Nightly image

There is also a nightly image, with the latest updates, which is built every
night from the Git repository. Be warned that this image is experimental and can
contain a lot of bugs or corrupt your data, as the `master` branch can be
instable from time to time. This image will fetch the latest sources from the
online Git repository and will not use any local sources.

To build a Nightly image built for a use in production:

    :::bash
    make docker-nightly

This command will build an image named `bnjbvr/kresus-nightly`.

Then, to start it, you can use the same `docker run` command as before, with
the correct image name:

    :::bash
    docker run -p 9876:9876 \
        -e LOCAL_USER_ID=`id -u` \
        --restart unless-stopped \
        -v /opt/kresus/data:/home/user/data \
        -v /opt/kresus/weboob:/weboob \
        -v /opt/kresus/config.ini:/opt/config.ini \
        --name kresus \
        -ti -d bnjbvr/kresus-nightly

## Installation within Yunohost

A [Yunohost](http://yunohost.org/) [app](https://github.com/YunoHost-Apps/kresus_ynh)
is available. It is still new and experimental, please report us any problem
you might encounter with it.

## Installation within ArchLinux

You can use `pacman` to install [Kresus for ArchLinux](https://www.archlinux.org/packages/community/x86_64/kresus/):

```bash
pacman -Syu kresus
```


## Requirements for other install methods

Kresus makes us of [Weboob](http://weboob.org/) under the hood, to contact
your bank website. You will have to [install Weboob core and
modules](http://weboob.org/install) so that the user running Kresus can use
them.

Kresus requires the latest stable version of Weboob. Although Kresus could
work with previous versions, bank modules can get obsolete and the
synchronisation with your bank might not work with older versions.


## Standalone install

**IMPORTANT NOTE: There is no builtin authentication mechanism in Kresus, it is
then risky to use it as is. Please make sure to set up an authentication system
by yourself.**

The following will install remaining dependencies, build the project and
install Kresus in the global Node folder. Note that if this folder is
`/usr/local/bin`, you will likely have to run the command as root.

### Local install

To install Node dependencies and build the scripts (this will not install
Kresus globally) for a production use:

    :::bash
    npm install && npm run build:prod

You can then run Kresus using:

    :::bash
    NODE_ENV=production npm run start

_Note :_ You can also use `npm run build:dev` to build the scripts in
development mode (better for debugging, but should not be used in production).


## Global install

Alternatively, if you want to install Kresus globally, you can use:

    :::bash
    make install

You will then be able to start Kresus from any terminal from any folder using:

    :::bash
    kresus


## Configuration

### Using a `config.ini` file

You can set the options to use in a `INI` file and pass the command-line
argument `-c path/to/config.ini` to Kresus. A `config.ini.example` file is
available in the Git repository to list all available options. It can be
copied and edited to better match your choices.

**Security :** In production mode (`NODE_ENV=production`), the configuration
folder should provide only read or read/write rights to its owner, using the
filesystem permissions. Otherwise, Kresus will not start.


### Using environment variables

Note that every configuration option has a matching environment variable: if
the environment variable is set, it will overload the configuration from the
`INI` file or the default value. Please refer to the `config.ini.example` file
to find the available environment variables.


## Firewall recommendations

You'll need the following firewall authorizations:

* http/https access to your bank website, for fetching new operations on your
behalf.
* http/https access to the Weboob repositories, for automatically updating the
bank modules before automatic pollings.
