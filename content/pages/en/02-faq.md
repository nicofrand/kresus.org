Title: FAQ
Date: 2017-03-11 10:02
Slug: faq
Summary: FAQ
toc_run: true
toc_title: Frequently Asked Questions
lang: en

## Daily use

### My account balance is incorrect in Kresus. What should I do?

This can happen from time to time has the underlying software used by Kresus,
Weboob, only gives an instantaneous view of the banking data from your bank
website, while Kresus tries to reconstruct the entire history. If data
provided by your bank are not coherent across time (some data change), then
the balance of one of your accounts might be incorrect in Kresus.

We advise you have a look at the "Duplicate" section in Kresus. If some
operations seem to be duplicates, this page will show them and you will be
able to merge them, by hand, to get back a correct balance. However, the way
of doing it is quite rudimentary and some operations marked as duplicates
could actually not be duplicates. It is not possible to cancel the merge of
two operations, hence take care when removing duplicates!

In the future, it will be possible to cancel the merge of duplicates.
Automatic detection and merge of duplicates should also be greatly improved.

### How can I manually add a transaction ?

It can sometimes be useful to create transactions before then even appear on
the bank's website and thus be fetched by Kresus.

To manually add a transaction, go to the transaction list and click on
”Add an operation” (“+” on mobile).

![Add transaction button](../images/pages/en/add-transaction-button.png)

If you create it on an account that is automatically updated through the
connection to your bank's website, once the real transaction appears on the
website and is retrieved by Kresus, a duplicate transaction should be
generated, that you'll have to manually merge. You might have to use the
“Find more” button if the date of the manual transaction is far enough from
the one of the actual transaction.

![Add transaction screen](../images/pages/en/add-transaction-modale.png)

## Security

### How is my banking data stored in Kresus?

If you use the standalone version of Kresus, we assume you know how to secure
your server. No data will be encrypted and this mode, not even your banking
password. Then, we advise you encrypt the hard disk on which Kresus is
installed, or you containerize the web service, or you use any such mean to
secure your password. In the future, the password might be stored encrypted in
this mode as well.

### Why should I give my banking password to Kresus?

To fetch banking operations from your bank website, Kresus fakes a web browser
(thanks to Weboob) and then login to your bank website using your credentials.
This lets him fetch your banking data, in an automated way, every night,
without any action from your part.

### Why does Kresus does not support two-factor authentication?

As Kresus logs in your bank website the same way as you would do through your
browser, but it does it every night, it should ask you every time to type your
two-factor authentication login, which is an obvious issue : Kresus would no
longer be autonomous.

## About Kresus

### I have a question which is not in this list but should be, in my opinion.

Great! Please ask it on our [community website](http://community.kresus.org/)
or [open an issue](https://framagit.org/kresusapp/kresus.org/issues/new) on our
bug tracker directly. If you have a beginning of answer, it's even better! If
you are wondering about something, chances are you are not the only one!

### How to report a bug?

Kresus is made by humans, and can then have some bugs! If you ever find an
issue while using Kresus, feel free to [open an
issue](https://framagit.org/kresusapp/kresus/issues/new) on our bug tracker.
Try to include as much information as you can on your setup: which Kresus
version are you using? Which Weboob version are you using? What are the steps
to reproduce the bug? The more information we have, the greater the chances to
see the bug fixed quickly!

### I'd like to have a new feature.

We are never short of ideas, and they are always greatly appreciated! You can
also [open an issue](https://framagit.org/kresusapp/kresus.org/issues/new) for
a feature request and even start to implement it! Maintainers are always happy
to help someone get started working on Kresus.

### Who are you?

Kresus being a <em>libre</em> software, we are a [communauty of
developpers](https://framagit.org/kresusapp/kresus/graphs/master) to code it.
We are not affiliated in anyways to CozyCloud or Weboob and are independant
developpers, interested in <em>libre</em> softwares and personal finance
managers.
