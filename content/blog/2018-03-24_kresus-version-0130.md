Title: Kresus : version 0.13.0
Date: 2018-03-25 14:00
Lang: fr
Slug: kresus-version-0-13-0

L'équipe vient de publier une nouvelle version de [Kresus](https://kresus.org),
un gestionnaire de finances personnelles open-source, libre et
auto-hébergeable. Pour rappel, cette application web, basée sur
[Weboob](http://weboob.org/) et disponible [à
l'installation](https://kresus.org/install.html) de manière autonome, dans
Docker ou dans [Yunohost](http://yunohost.org/), vous permet de vous connecter
à vos comptes bancaires, de récupérer vos opérations et de les classifier pour
savoir comment vous dépensez votre argent afin de mieux maîtriser vos finances.

## Trop Long, Pas Lu

- Kresus requiert désormais Node 6 au minimum, ainsi que Weboob version 1.3.
- La liste de diffusion kresus@framalistes.org ne sera plus utilisée pour
  communiquer, celle-ci est remplacée par [notre
  forum](https://community.kresus.org/).
- Si vous utilisiez les courriels avec un serveur SMTP, précisez
  `transport=smtp` dans la section `[email]` de votre fichier de configuration
  `config.ini`.
- Nouveau thème *light*, meilleure procédure de démarrage pour les nouveaux et
  nouvelles arrivant⋅e⋅s, possibilité d'exclure des comptes de la balance d'une
  banque.

## Les (grosses) nouveautés !

Cette version est la première version qui ne supporte plus
[CozyCloud](http://cozy.io/), dû à des choix techniques effectués par
CozyCloud. Si vous utilisez encore Kresus dans Cozy, nous vous recommandons
d'utiliser [Yunohost](http://yunohost.org/) ou Docker ou de passer à une
installation directe pour bénéficier de cette dernière version. Les
instructions sont présentes sur [Kresus.org](https://kresus.org/install.html).

L'interface de Kresus a connu un certain nombre d'améliorations dont
l'apparition d'un gestionnaire de thèmes. Pour fêter l'arrivée de cette
fonctionnalité, un nouveau thème *light* a été rajouté, plus léger, plus
subtil, plus clair. Une icône de chargement est affichée en attendant la
première récupération des données, à la place de l'écran blanc qu'il y avait
précédemment. La section de doublons affiche plus d'informations sur les
opérations affectées et demande désormais une confirmation avant la fusion.
Elle est dorénavant utilisable sur mobile également. L'accueil des nouveaux
utilisateur⋅ice⋅s a été totalement repensé et simplifié pour mieux accompagner
à la mise en place de ses comptes. Il est maintenant possible de créer
automatiquement un jeu d'alertes et de catégories par défaut à l'ajout du
premier compte. Si vous utilisez déjà Kresus et que vous désirez profiter du
jeu de catégories pré-établies, pas de panique ! Il est également possible de
les créer plus tard depuis la section des catégories.

<video width="560" height="315"
    controls
    src="{static}/videos/kresus-13-onboarding.mp4"
    allowfullscreen></video>

Si vous avez un prêt en cours, vous avez peut-être un compte avec une balance
négative très élevée, et Kresus vous affiche une balance disponible dans cette
banque très basse. Il est possible d'exclure un compte du calcul de la balance
de banque. Pour cela, allez dans la section Préférences, puis cliquez sur la
petite calculette située à côté du nom du compte que vous souhaitez exclure.

<video width="560" height="315"
    controls
    src="{static}/videos/kresus-13-exclude.mp4"
    allowfullscreen></video>

Vous recevez votre salaire en fin de mois et vous préféreriez qu'il soit
affecté au budget du mois suivant pour vos calculs de budget ? Vous pouvez
désormais affecter une opération au mois précédent ou suivant, en cliquant sur
les boutons de la petite fenêtre qui présente les détails d'une opération.

![Affectation des budgets]({static}/images/blog/kresus-013-budget-mois-suivant.png)

## Les autres nouveautés

Une nouvelle "banque" est supportée : `myedenred`, qui permet de suivre votre
solde de carte de Ticket Restaurant©. Wells Fargo n'est plus à jour et
supportée par Weboob et n'est donc plus utilisable dans Kresus.

Les accès bancaires sont désormais triés dans le menu : d'abord vient l'accès à
la banque ayant le compte affiché par défaut (marqué d'une étoile dans les
préférences), puis les accès actifs dans l'ordre alphabétique et enfin les
accès inactifs.

L'interaction avec les banques a été totalement retravaillée et est plus rapide
et plus stable. De nombreux problèmes ont été corrigés, notamment pour les mots
de passe contenant des caractères accentués ou des espaces.

Sur mobile, il est maintenant possible d'ajouter Kresus à votre écran
d'accueil, avec les versions récentes de Firefox et de Chrome pour Android.

Pour faciliter la gestion et le débogage de votre instance, les journaux
serveur sont dorénavant directement accessibles depuis l'interface de Kresus,
dans l'onglet "Journaux" des Préférences. Par mesure de sécurité, les journaux
sont anonymisés dans l'interface web, ce qui permet de les copier / coller pour
effectuer des rapports de bug.

## Pour les sysadmins

Avec cette nouvelle version, Kresus requiert que Weboob soit installé en
version 1.3 au minimum. Cette version est étiquetée stable et les précédentes
ne sont officiellement plus supportées. De même, cette version de Kresus
requiert Node en version 6 ou plus, node v4 n'étant plus supporté.

Les notifications par courriel peuvent désormais être envoyées soit en
utilisant un serveur SMTP comme c'était déjà le cas jusqu'à présent, soit en
utilisant `sendmail` si le serveur sur lequel tourne Kresus est capable
d'envoyer des courriels. Ce changement requiert une action manuelle de votre
part en éditant le fichier config.ini pour définir le [type de transport à
utiliser](https://framagit.org/bnjbvr/kresus/blob/404e4d06ad655645c264e1a1f03dfeba57fc216c/docker-compose/config.ini#L62-71).

Les documentations d'installations ont été revues et déplacées vers
[kresus.org](https://kresus.org/install.html).

## Le coin du développement

Une nouvelle commande dans le `Makefile` fait son apparition : `make dev`,
inspirée d'une règle similaire dans
[PeerTube](https://github.com/Chocobozzz/PeerTube). Cette commande compile les
parties cliente et serveur en mode `watch` (pour recompiler automatiquement
lorsque vous modifiez le code) et sert l'application avec un petit serveur web
embarqué (sur le port `8080` par défaut) qui rechargera automatiquement
l'application à la recompilation.

De nouveaux identifiants spécifiques sont utilisables dans le faux module
bancaire reposant sur Weboob, "Fake Weboob Bank", pour le développement et les
démos, pour sélectionner finement les erreurs levées.

Enfin, pour faciliter le développement et l'harmonisation du style de code dans
les quelques parties du code en Python, il y a désormais une intégration de
`pylint`. `prettier` s'occupe quant à lui de la normalisation du style de code
du JavaScript.

## Le mot de la fin

Merci à tous les contributeur⋅ice⋅s pour leurs très nombreuses contributions !
Remercions [Arnaud](https://github.com/ArnoBouts) en particulier ce mois-ci,
qui a contribué ses premiers patches lors de cette version, bravo à lui !

Nous avons par ailleurs migré l'ancienne liste de diffusion, peu utilisée, vers
un forum basé sur Discourse disponible sur
[http://community.kresus.org/](http://community.kresus.org/).

Si vous avez des retours à nous faire, des suggestions ou des remarques,
n'hésitez-pas à nous le faire savoir, sur [notre
forum](https://community.kresus.org),
[mastodon](https://tutut.delire.party/@kresus),
[twitter](https://twitter.com/kresusapp), ou le [canal
IRC](https://kiwiirc.com/client/chat.freenode.net/kresus) !
