Title: Kresus : version 0.12.0
Date: 2017-10-21 14:00
Lang: fr
Slug: kresus-version-0-12-0

L'équipe vient de publier une nouvelle version de [Kresus](https://kresus.org),
un gestionnaire de finances personnelles open-source, libre et
auto-hébergeable. Pour rappel, cette application web, basée sur
[Weboob](http://weboob.org/) et disponible [de manière
autonome](https://www.karolak.fr/blog/2016/03/18/kresus-un-gestionnaire-web-de-finances-personnelles/),
dans [Yunohost](http://yunohost.org/) (**nouveau !**), ou dans
[CozyCloud](https://cozy.io/) (version 2), vous permet de vous connecter à vos
comptes bancaires, de récupérer vos opérations et de les classifier pour savoir
comment vous dépensez votre argent afin de mieux maîtriser vos finances.

## En résumé

Si vous utilisez les emails en mode standalone, une action manuelle de votre
part est nécessaire : rendez-vous à la partie Configuration de cet article !

C'est officiellement la dernière version à fonctionner au sein de CozyCloud :
en effet, la version node de Cozy n'est plus maintenue, et d'ici la future
publication de Kresus, l'ensemble des utilisateurs hébergés par Cozy aura été
migré sur la prochaine version majeure de Cozy avec laquelle Kresus ne sera
techniquement pas compatible.  Mais pas de panique ! LowMemory a contribué à la
création d'un package [YunoHost](http://yunohost.org/) en beta pour pouvoir
installer Kresus très simplement ! Tous les retours sont très appréciés sur [le
forum de Yunohost](https://forum.yunohost.org/t/kresus-app-please-test/3546)
directement. Un immense merci à LowMemory!

La prochaine version de Kresus abandonnera le système d'émulation de base de
données *pouchdb* pour préférer l'utilisation d'une base de donnée SQL plus
classique. Un outil de migration manuel sera proposé avec la nouvelle version.

Côté code, cette nouvelle version de Kresus voit apparaître 173 commits,
provenant de 5 contributeurs différents (ainsi qu'un
[robot](https://github.com/singapore/renovate)).

## Nouvelles fonctionnalités

### Désactivation d'un accès

Il est désormais possible de désactiver un accès à une banque, par exemple
suite à la fermeture d'un compte (plus aucune synchronisation ne sera réalisée
avec le site de la banque). L'historique des opérations bancaires de tous les
comptes d'un accès désactivé est conservé, mais le mot de passe n'est plus
stocké dans la base de données et les synchronisations ne sont plus possibles,
manuelles ou automatiques.

Pour désactiver un accès, il suffit d'aller dans les paramètres, et de cliquer
sur le bouton 'éteindre' sur la ligne du compte en question. Pour le réactiver,
il suffit de cliquer à nouveau sur ce bouton, confirmer les informations de
connexion et ressaisir votre mot passe. La synchronisation automatique
recommencera alors normalement.

### Routage

L'interface utilisateur de Kresus est maintenant synchronisée avec l'URL
(routage). Il est donc possible de créer des raccourcis navigateur pour chacun
de ses comptes, ou pour n'importe quel écran de Kresus. Pour les utilisateurs
qui accèdent à Kresus via un sous-répertoire d'un domaine, donc avec une url du
type mon.domaine.fr/kresus, il faudra déclarer le chemin d'accès à
l'application (/kresus dans ce cas) dans le fichier de configuration (décrit
ci-dessous).

### Amélioration de l'expérience utilisateur

La gestion des erreurs et du log de Weboob a été reprise afin d'être plus
cohérente et de mieux notifier les erreurs à l'utilisateur. Les messages
d'erreur sont ainsi plus utiles et facilitent le débogage ! Kresus vous
avertira désormais tout seul quand le site de votre banque requiert une action
de votre part et bloque la synchronisation (dans le cas des pubs d'ING direct,
par exemple).

Il est désormais possible d'afficher, de manière temporaire, un mot de passe
qui a été entré auparavant, afin de vérifier que l'on n'a pas fait de faute de
frappe.

Les panneaux dépliables (recherche d'opérations, création d'accès...) ont été
harmonisés : ils ont maintenant le même comportement, et le contenu des
formulaires est maintenu lorsque le panneau est caché puis montré de nouveau.

Sur mobile, la modale montrant les détails d'une opération peut désormais être
ouverte en effectuant un appui long sur le nom de l'opération.

## Amélioration des performances

Un important travail sur les performances a été commencé dans cette version et
son impact devrait d'ores et déjà se faire sentir sur les configurations les
plus faibles (Raspberry Pi notamment), et sur toutes les machines au chargement
du client.

### Application web

Des optimisations pour rendre l'application web plus fluide ont été engagées
(en diminuant les mises à jour du DOM). A terme, ceci permettra d'avoir une
interface encore plus rapide et plus agréable, notamment lors du parcours de la
liste des opérations sur mobile. Ces premières optimisations ne sont qu'un
début, et les changements devraient se poursuivre dans les versions suivantes.

Les scripts nécessaires à la génération  des graphiques sont désormais chargés
en décalé une fois que le reste de l'application est prêt, ce qui permet donc
un chargement plus rapide de l'application.

### Démarrage plus rapide du serveur

Les migrations de la base de données étaient auparavant toutes lancées à chaque
démarrage du serveur, ce qui retardait d'autant le démarrage effectif du
service. Dorénavant, seules les nouvelles migrations seront lancées, afin
d'avoir un démarrage plus rapide !

La vérification de l'installation et la détection de version de Weboob ne sont
plus faites qu'en cas d'absolue nécessité, et non plus systématiquement comme
auparavant, pour diminuer les temps de réponse du serveur.

## Administration et déploiement

### Configuration

Une partie de la configuration (notamment celle du serveur SMTP, et des
variables d'environnement relatives à Weboob) a été migrée depuis la base de
données dans un fichier de configuration. Un exemple de fichier est fourni dans
le fichier `config.example.ini`. S'il est fourni, ce fichier de configuration
ne doit être accessible en lecture que par l'utilisateur unix qui lance le
processus Kresus.

**Pour les utilisateurs de la version standalone (sans Cozy)** :  il est
nécessaire de paramétrer les informations d'envoi d'emails dans le fichier de
configuration, les informations en base de donnée ne seront plus utilisées (à
part l'adresse email du destinataire).

Pour lancer Kresus avec le fichier de configuration, il suffit d'utiliser la
commande suivante :

 `./bin/kresus.js --config chemin/vers/le/fichier/de/conf.ini`

Il est toujours possible de surcharger la configuration en définissant les
variables d'environnement adéquates à l’exécutable ; les variables
d'environnement sont prioritaires sur les valeurs du fichier de configuration.
Cela permet par exemple d'avoir une configuration par défaut pour un parc
d'instances de Kresus, ainsi que des valeurs spécifiques pour chaque instance.
Voir `config.example.ini` pour connaitre les options possibles et les variables
d'environnement équivalentes.

### Weboob

La dernière version de [Weboob](http://weboob.org/) (1.3) est désormais
compatible avec Python 3 (sous réserve que le module utilisé le soit aussi). Le
code pour interfacer Kresus avec Weboob a donc été réécrit en conséquence, afin
de pouvoir utiliser Python 3 avec Kresus. Python 2 est cependant toujours
utilisé par défaut dans cette version de Kresus. Pour utiliser Python 3 ou un
virtualenv Python, il suffit d'installer la version adéquate de Weboob, puis
d'indiquer l'exécutable Python à utiliser dans le fichier de configuration ou
via la bonne variable d'environnement.

Kresus gère désormais lui-même le fichier `sources.list` de Weboob, spécifiant
les chemins d'où charger les modules. En utilisant votre propre fichier
`sources.list` plutôt que le fichier par défaut, vous pouvez utiliser [une
copie locale des modules](https://git.weboob.org/weboob/modules) et gérer les
mises à jour manuellement, plutôt que d'utiliser le mécanisme de mises à jour
automatiques des modules de Weboob. Cette fonctionnalité s'active via le
fichier de configuration ou la bonne variable d'environnement.

### Docker

Les images Docker de Kresus mettent désormais à jour Weboob avant de démarrer
Kresus. Si vous rencontrez des problèmes de modules, un `restart` de votre
instance Docker pourrait régler le problème.

Il existe désormais plusieurs images Docker reconstruites à chaque modification
du code sur la branche de développement, appelées Nightly. Ces versions
contiennent les toutes dernières fonctionnalités, mais aussi quelques bugs
potentiels. C'est idéal si vous êtes vraiment avide de nouvelles
fonctionnalités et acceptez d'essuyer les plâtres tout en nous faisant des
retours afin que nous puissions assurer la meilleure stabilité de
l'application. Ces images sont accessibles dans le Hub Docker sous les noms
suivants :

- **bnjbvr/kresus-nightly:prod** : c'est l'image Nightly la plus rapide et
  recommandée pour l'utilisation au quotidien.
- **bnjbvr/kresus-nightly:dev** : les fichiers ne sont pas optimisés pour
  la production. Cela permet un debug plus aisé mais les fichiers peuvent
  être plus lents à charger

Des dépendances nécessaires au bon fonctionnement des modules Weboob ont été
ajoutées aux images Docker.

## Pour les développeurs

### Changement du système de *build*

Tout le code client est dorénavant compilé en utilisant Webpack. Cette
modification s'accompagne d'une meilleure minification des fichiers servis,
d'une plus grande souplesse dans la compilation, en éliminant le besoin de
scripts dédiés, ainsi que le découpage du code client en plusieurs fichiers
compilés. Ce changement de système de build permet aussi le chargement
asynchrone de code.

Le système d'intégration continue a également été modifié suite au passage à
Webpack, afin d'offrir une meilleure couverture des tests et une plus grande
stabilité de Kresus à terme.

### API

Cette version apporte quelques nouveautés sur l'API, en déplaçant tous les
points d'entrée sous une même racine `/api/v1`. Le travail sur l'API devrait se
poursuivre dans les prochaines versions afin de documenter une API propre
utilisable par d'autres clients que l'application web officielle.

Il est par ailleurs possible de manuellement lancer une récupération de toutes
les opérations sur tous les comptes actifs via le point d'accès `GET
/api/v1/accesses/poll`. Vous pouvez de cette manière provoquer plusieurs
récupérations de vos comptes et opérations par jour en ligne de commande ;
cette fonctionnalité sera intégrée plus tard dans l'interface graphique.

## Le mot de la fin

Merci à tous les contributeurs, et en particulier aux hyperactifs
[nicofrand](https://nicofrand.eu), [ZeHiro](https://github.com/ZeHiro) et
[Phyks](https://phyks.me/) pour avoir écrit ce billet et pour leurs très
nombreuses contributions !

Kresus se pare désormais de son propre site web (sans blague, vous êtes dessus)
et de son propre [forum de discussion](https://community.kresus.org/). Si vous
avez des retours à nous faire, des suggestions ou des remarques, n'hésitez-pas
à nous le faire savoir, sur [notre forum](https://community.kresus.org) donc,
[mastodon](https://tutut.delire.party/@kresus),
[twitter](https://twitter.com/kresusapp), le [canal
IRC](https://kiwiirc.com/client/chat.freenode.net/kresus), la [liste de
diffusion](https://framalistes.org/sympa/info/kresus), ou encore
[diaspora](https://framasphere.org/people/315a5640ead10132c4cc2a0000053625) !
