Title: Kresus : version 0.14.0
Date: 2019-02-24 19:07
Author: Les mainteneurs de Kresus
Lang: fr
Slug: kresus-version-0-14-0

C'est l'heure de sortir une nouvelle version de Kresus ; et oui, ça faisait
presque [un an](https://kresus.org/blog/kresus-version-0-13-0.html) depuis la
précédente version stable !

On a déjà évoqué la plupart des nouveautés de cette version dans [la
rétrospective de 2018](https://kresus.org/blog/2018-retrospective.html) mais ce
billet est l'occasion de vous les présenter plus précisément, avec quelques
images et explications.

Si vous êtes pressés et ne désirez que l'essentiel, rendez-vous dans la toute
dernière section ["Le point administration
système"](#le-point-administration-systeme) pour connaître les nouveaux besoins
de Kresus !

## Une meilleure gestion des comptes

### Renommer un compte et/ou un accès

Il est désormais possible de renommer un compte avec un libellé personnalisé,
pour ne plus avoir des *COMPTE CHEQUE MONSIEUR MADAME UNETELLE* mais quelque
chose de plus parlant comme *Compte joint*. De même, si vous avez plusieurs
accès bancaires dans la même banque, vous pouvez maintenant les distinguer en
leur donnant des noms différents. Rendez-vous dans les préférences pour
utiliser cette fonctionnalité !

### Des nouvelles banques et des changements au niveau d'autres banques

Le support pour de nouvelles banques a été ajouté : la Banque Européenne du
Crédit Mutuel, Binck, Bolden, Nalo, les assurances-vie Suravenir et la Nef.
Certains de ces modules sont récents, donc n'hésitez-pas à nous faire [des
retours](http://community.kresus.org/) en cas de soucis de récupération des
données.

La connexion à certaines banques pouvait avoir cessé de fonctionner dans
Kresus, comme par exemple avec la Banque Populaire ou BNP Paribas. Cela devrait
avoir été corrigé dorénavant, et nous vous invitons à tester le support de ces
banques (et à [nous faire signe](http://community.kresus.org/) en cas de
souci).

Certaines banques n'étant plus supportées par Weboob, l'outil qui se connecte à
votre place au site de votre banque. Elles ont donc été dépréciées, mais pas de
panique ! Aucune suppression de donnée n'aura lieu au sein de votre Kresus, et
vous pourrez toujours consulter ces comptes en lecture seule.

### Créer des comptes gérés à la main

Jusqu'à présent, Kresus vous donnait uniquement la possibilité de gérer des
comptes en vous connectant aux accès en ligne de vos banques. Depuis cette
nouvelle version, il est également possible de créer des comptes "manuels", qui
vous permettront d'ajouter vous-même toutes les transactions à la main, de
nommer vos comptes, etc.

<video width="100%" height="315"
    controls
    src="{static}/videos/kresus-14-banque-manuelle.mp4"
    allowfullscreen></video>

Pour ajouter un accès manuel, allez dans les préférences dans l'onglet «
Banques » comme pour toute autre banque et cherchez « Manual bank ».  Un
identifiant et mot de passe vous seront demandés, saisissez ce qu'il vous
plaît, cela n'a aucune importance : Kresus n'est pas encore à même de
comprendre qu'un accès peut ne pas nécessiter d'identifiants (on va bien sûr
travailler pour rendre ça plus simple et plus clair !).

Lors de la création vous aurez un nouvel accès bancaire créé avec trois comptes
: deux en euros et le dernier en dollars US. Vous pouvez bien sûr supprimer les
comptes non nécessaires, les renommer, etc. Dans le futur, il sera possible
d'ajouter d'autres comptes à cet accès, en le paramétrant aux petits oignons
selon vos goûts.

### Ajouter une opération manuelle directement depuis la liste d'opérations

Le bouton existait déjà mais était caché dans les préférences. Retrouvez-le
désormais directement dans la vue « Relevé » au même niveau que la date de
dernière synchronisation de votre compte, aussi bien sur ordinateur que sur
mobile.

Si c'est un compte alimenté automatiquement via un connecteur qui se connecte à
votre banque, assurez-vous que votre compte est bien à jour avant de créer
cette opération. Si vous la créez en prévision d'une opération qui sera
importée depuis votre banque, cela risque de générer des doublons qu'il faudra
plus tard fusionner manuellement.

## Des sélecteurs plus intelligents

Dans la liste d'opérations, les sélecteurs de catégories et de types essaient
désormais de prédire intelligemment ce que vous désirez écrire ; vous pouvez
écrire les premières lettres d'une catégorie (ou n'importe quelles lettres
présentes dans n'importe quel ordre) pour que Kresus vous propose le nom
complet, ou vous pouvez directement créer de nouvelles catégories depuis la
même interface.

![Capture d'écran animée d'un sélecteur de catégorie]({static}/images/blog/014-smart-selectors.gif)

## Une gestion des doublons plus simple

### Un aperçu du nombre de doublons dans le menu

![Capture d'écran du menu de Kresus montrant 1 doublon]({static}/images/blog/014-duplicates-menu.png)

Le nombre de doublons pour le compte en cours d'observation est désormais
affiché dans le menu de doublons directement.

### Meilleure détection des doublons lors des récupérations

La détection des doublons lors de l'import des données a été amélioré. Cela
devrait résulter en un nombre de doublons importés à tort plus bas. D'autres
améliorations seront apportées afin de diminuer encore le nombre de doublons,
pour rendre cela le plus simple et faire oublier la notion de doublons au
maximum.

### Disqualifier des faux doublons

Il est maintenant possible de disqualifier des paires d'opérations présentées
comme des doublons en utilisant les champs saisis manuellement par
l'utilisateur (libellé personnalisé par exemple). En saisissant un libellé
personnalisé différent à chacune des deux opérations détectées à tort comme des
doublons, il est possible de les faire disparaître de la liste des doublons.

Ce nouveau comportement est activé par défaut. Si jamais vous avez cependant un
doute et que vous désirez le désactiver, aucun souci ! Rendez vous dans la
fenêtre des paramètres par défaut de la section Doublons.

## Un système de thèmes pour personnaliser l'interface

Si vous vous lassez un peu de l'interface de Kresus, vous pouvez en changer le
style depuis les préférences, dans l'onglet « Thèmes ». Deux thèmes
additionnels vous sont proposés : « light », qui se veut une version plus
épurée de l'interface, et sa version sombre si vous désirez économiser votre
batterie : « dark ».

Thème *light* :
![Capture d'écran du thème light]({static}/images/blog/014-light.png)

Thème *dark* :
![Capture d'écran du thème dark]({static}/images/blog/014-dark.png)

## Des graphiques plus précis

Tous les graphiques arborent désormais un sélecteur de période, afin de pouvoir
zoomer plus finement sur une période donnée :
![Un sélecteur de période dans les graphiques]({static}/images/blog/014-range-slider.png)

Les graphiques par catégorie affichent désormais deux types de graphiques en
camemberts, quand le mode sélectionné contient à la fois les dépenses et les
rentrées d'argents : les camemberts *nets* et *bruts* (noms à améliorer ; nous
serions friands de vos idées). Ceux-ci montrent les dépenses / rentrées
cumulées ensemble (pour les nets) ou séparées (pour les bruts). Voir les
détails directement dans l'aide intégrée à ces nouveaux graphiques en
camemberts !

## À propos de l'à propos

![Capture d'écran de la section à propos]({static}/images/blog/014-apropos.png)

La nouvelle section "à propos" vous donne des informations sur le projet,
comment rentrer en contact avec la communauté et liste également les nombreux
autres projets libres sur lesquels s'appuie Kresus. Merci à tous ces projets et
tou.te.s leurs contributeur.ice.s !

## Des exports chiffrés

Sous réserve que vous ayez configuré un *sel* (une sorte de clé aléatoire de
minimum 16 caractères) dans votre fichier `config.ini` dans la section
`[kresus]`, Kresus vous proposera désormais de chiffrer votre export de données,
ou de déchiffrer votre import.

Si vous choisissez de le chiffrer, vos identifiants bancaires seront eux aussi
exportés. Sinon ils seront ignorés et il vous faudra les saisir de nouveau
après l'import, comme à l'heure actuelle.

![Capture d'écran de l'export chiffré de Kresus]({static}/images/blog/014-encrypted-export.png)

⚠️ Attention, si vous souhaitez importer un fichier chiffré, le *sel*
sur l'instance d'import doit être identique à celui sur l'instance d'export !
Sinon, re-rentrer votre mot de passe constitue un acte nécessaire pour
s'assurer de la confiance que vous avez en votre nouvelle instance.

Pour générer un *sel* facilement, vous pouvez par exemple utiliser l'utilitaire
en ligne de commande de `openssl` :

    :::bash
    openssl rand -base64 32


## Sous le capot

* Nous avons arrêté d'utiliser la bibliothèque de code *Bootstrap* pour
  effectuer notre design, pour passer à du code CSS écrit à la main, plus
  maintenable, plus simple, plus moderne. Un grand merci à
  [Bootstrap](https://getbootstrap.com/) pour ses loyaux services, et bonne
  retraite ! Et un grand merci à Nicofrand qui a été l'instigateur principal de
  ce gros changement.
* Notre code côté serveur a commencé à être migré pour séparer les données par
  utilisateur, dans le but d'apporter (plus tard !) un support
  multi-utilisateur. Un grand merci à ZeHiro pour le travail effectué là-dessus
  !
* Toutes les migrations de données (effectuées d'une version à une autre) sont
  désormais testées à la main, pour éviter des mauvaises surprises.

## Le point administration système

* Il s'agit d'une grosse mise à jour, il est donc conseillé d'effectuer une
  sauvegarde de vos données avant la mise à jour, en sauvegardant le dossier
  des données de Kresus entièrement (qu'il sera possible de restaurer par un
  écrasement brut des données).
* Comme indiqué plus haut, il est nécessaire d'ajouter un *sel* utilisé pour le
  chiffrement des mots de passe dans le fichier `config.ini` de votre instance,
  si vous désirez autoriser les imports et exports chiffrés.
* La version de [Weboob](http://weboob.org/) nécessaire est désormais la
  dernière version stable 1.4. Le paquet Debian n'étant plus maintenu, il est
  préférable de l'installer via `pip` avec la commande `pip install weboob`.

## Merci !

Kresus ne pourrait exister sans ses nombreux.se.s contributeur.ice.s ! Côté
conception, nous avons pu effectuer nos premiers tests expérience utilisateur,
et avoir le soutien de plusieurs designers pour nous aider à rendre certaines
choses plus simples. Côté documentation, nous avons pu avoir des exemples de
configuration
[nginx](https://framagit.org/kresusapp/kresus/blob/master/support/nginx.conf)
et des nouveaux tutoriels (rendez-vous dans
[l'aide](https://kresus.org/install.html) en ligne). Côté développement, cette
nouvelle version contient pas moins de 481 *commits* (unités de travail en
code) depuis la version précédente, effectués par 10 contributeur.ice.s ! Un
grand merci à tout le monde !

Si vous avez des retours à nous faire, des suggestions ou des remarques,
n'hésitez-pas à nous le faire savoir, sur [notre
forum](https://community.kresus.org),
[mastodon](https://tutut.delire.party/@kresus),
[twitter](https://twitter.com/kresusapp), ou le [canal
IRC](https://kiwiirc.com/client/chat.freenode.net/kresus) !
